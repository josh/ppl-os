#!/bin/bash

die() {
  echo -e "ERROR: $1" 1>&2
  exit 2
}

if [ "x$1" == "xread" ]; then
  if [ ! -e .config ]; then
    die "No config found. Run '$ bash conf-tool.sh'"
  fi
  . .config
  echo "${!2}"
  exit
fi

signing_key=$(snap keys | tail -n 1 | sed "s| |\n|g" | head -n 1) # value if no key exists: "No"
user_email=$(snap whoami | sed "s|email: ||g") # value if not signed in: "-"

read_prompt() {
  read -p "> Enter $1 (default $2$3): " val
  if [ -z "$val" ]; then
    val="$2"
  fi
  echo "< $val"
}

if [ "$user_email" == "-" ]; then
  die "User email is not set up. Please run:\n\n\t$ snap login"
fi

if [ "$signing_key" == "No" ]; then
  die "Signing key is not set up. https://tutorials.ubuntu.com/tutorial/create-your-own-core-image#3"
fi

dev_user=$(echo "$user_email" | sed "s|@.*||g")

read_prompt "Device channel" "edge" # TODO: switch to "stable"/"beta" once released for rpi3
channel="$val"
read_prompt "Device environment" "dev" " - use prod for production"
dev_env="$val"

read_prompt "Development device username" "$dev_user" " - taken from user email"
dev_user="$val"
read_prompt "Development device address"  "localhost" " - for qemu emulation"
dev_addr="$val"
read_prompt "Development device port"     "8022"      " - for qemu emulation, use 22 for real device"
dev_port="$val"

vals="CHANNEL=$channel
ENV=$dev_env
DEVICE_IP=$dev_addr
DEVICE_USERNAME=$dev_user
DEVICE_PORT=$dev_port
USER_KEY=$signing_key
USER_EMAIL=$user_email"

echo "
$vals
"

read -p "Is this correct [y/N]:" v
if [[ "$v" == [yY]* ]]; then
  echo "$vals" > .config
else
  echo
  bash $0
fi
