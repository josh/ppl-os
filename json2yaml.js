'use strict'

const yaml = require('js-yaml')
let d = []

process.stdin.on('data', data => d.push(data))
process.stdin.on('end', () => {
  console.log(JSON.stringify(yaml.safeLoad(Buffer.concat(d)), null, 2))
})
