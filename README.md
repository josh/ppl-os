# ppl-os

Files required to build the ubuntu core based pplOS

# WORK IN PROGRESS

Everything subject to change, including number of bugs (hopefully to zero ;)

# Building

## System Requirements

To build an image you need to have at least Ubuntu 16.04 on your machine and the following packages installed: `ubuntu-image snapd nodejs`

Additionally at least 1 GB of free disk space is required

For virtual device testing you also need `qemu-kvm`

Additionally some npm packages are required, run `npm i` to install them

## Other Requirements

 - Ubuntu One account (create at [https://login.ubuntu.com/+login](https://login.ubuntu.com/+login))
 - Snapcraft signing key (tutorial [https://tutorials.ubuntu.com/tutorial/create-your-own-core-image#3](https://tutorials.ubuntu.com/tutorial/create-your-own-core-image#3))
 - Any of the supported boards or at least 4GB RAM free for virtualization

## Creating the image

First run `make` once, it will guide you through the inital configuration

To build a board's model file run `make BOARD.model`

To build an image for a specific board run `make BOARD.img`

## Running the virtual device

Use `make start` to build the amd64.img and launch it with kvm

If you used the defaults in the configuration step you should now be able to connect with `make ssh`

### `Could not access KVM kernel module: Permission denied`

Is your user in the kvm group? Try `sudo addgroup $(whoami) kvm` and reboot.
